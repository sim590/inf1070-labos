.PHONY: html

title = "INF1070"

html:
	pandoc README.md -s -V "pagetitle:$(title)" -V "title:$(title)" -f markdown -o README.html -c misc/github-pandoc.css
