# Laboratoire 1: Introduction à Linux

## Exercice 1: Environnement de bureau

L'objectif de cet exercice est de vous familiariser avec un environnement de
bureau Linux, en comparant les similitudes et les différences.

## 1.1: S'authentifier

Tout d'abord, connectez-vous sur la machine à votre disposition en tant
qu'utilisateur. Vous devez utiliser les informations suivantes:

- Nom d'utilisateur: *votre code MS*, composé de 2 lettres, suivies de 6
  chiffres.
- Mot de passe: *votre NIP*.

## 1.2: Navigateur de fichiers

En utilisant l'application de navigation de fichiers Nautilus, explorez votre
système.

- Repérez votre répertoire personnel (`home`), ainsi que les répertoires
  classiques (`Desktop`, `Documents`, `Downloads`, etc.).
- Dans le répertoire `Documents`, créez un répertoire dédié au cours nommé
  `INF1070` (ou tout autre nom de votre choix).
- Dans le répertoire `INF1070`, créez un sous-répertoire nommé `labo01` (ou
  tout autre nom de votre choix).

## 1.3: Fureteur et téléchargement

Ensuite, naviguez sur Internet avec un fureteur (Firefox ou Chromium, par
exemple).

Rendez-vous sur le site du cours: https://info.uqam.ca/~privat/INF1070/ et
cliquez sur le plan de cours pour le visualiser.

Ensuite, téléchargez les fichiers suivants et placez-les dans le répertoire
`labo01` que vous avez créé un peu plus tôt.

1. [Un fichier texte](https://www.gnu.org/licenses/gpl.txt),
2. [Du code source Java](./HelloWorld.java),
3. [Un fichier PDF](./cat.pdf),
4. [Un fichier PNG](ubuntu.png),
5. [Un fichier
   audio](https://upload.wikimedia.org/wikipedia/commons/a/a9/Bilabial_nasal.ogg).

## 1.4: Lecture de fichiers

Double-cliquez sur chacun des fichiers téléchargés. Notez le nom des
applications qui permettent de les lire.

Ensuite, ouvrez les mêmes fichiers à l'aide d'un éditeur de texte (par exemple
Gedit). Quels sont les fichiers textes? les fichiers binaires?

Lancez différentes applications qui apparaissent dans le menu, par exemple la
suite LibreOffice (Writer, Calc, Impress), qui sont des alternatives libres à
Microsoft Office:

1. Créez une feuille de travail avec Calc qui contient un tableau de 2 colonnes
   et 3 lignes.
2. Exportez votre document Calc au format PDF et ouvrez-le pour voir le
   résultat.

## 1.5: Paramètres systèmes

Explorez brièvement les *Paramètres système* (en anglais, *System settings*).

1. Modifiez la vitesse de la souris pour qu'elle corresponde à vos préférences.
2. Modifiez la résolution de votre écran.
3. Modifiez le clavier utilisé.
4. Changez l'image d'arrière plan de votre bureau.

## Exercice 2: Terminal

Lancez un terminal en cliquant sur l'icône correspondant.

1. Dans le menu, choisissez *Terminal* et ensuite *Préférences*. En jouant avec
   les différents choix proposés, choisissez des options qui vous conviennent
   (couleur d'arrière-plan, taille de la police, etc.). Notez l'encodage
   utilisé, qui devrait être UTF-8 par défaut.
2. Une fois satisfait de l'apparence de votre terminal, à l'aide des commandes
   `pwd`, `ls` et `cd`, naviguez dans votre système de fichiers. Repérez en
   particulier votre répertoire personnel *home*. Vous pouvez vous y rendre en
   tout temps directement en tapant `cd`.
3. Repérez les documents ainsi que les les fichiers multimédias que vous avez
   téléchargés à l'exercice précédent. Ouvrez-les à l'aide de la commande
   `xdg-open`.

Testez les commandes suivantes (n'hésitez pas à consulter la documentation à
l'aide de `man` si vous ne savez pas comment utiliser la commande):

4. La commande `date`, pour afficher des informations temporelles sur les
   fichiers téléchargés.
5. La commande `cat`, pour afficher le contenu des fichiers textes (TXT et
   Java) que vous avez téléchargés. En consultant l'aide avec la commande
   `man cat`, affichez les fichiers en numérotant les lignes. Que se passe-t-il
   lorsqu'on affiche le contenu d'un fichier binaire?
6. À l'aide de la commande `wget`, téléchargez à nouveau les fichiers
   mentionnés plus haut en ligne de commande. Notez que pour les fichiers
   hébergés sur [GitLab.com](https://gitlab.com/), il faut cliquer sur le
   bouton *Open raw* pour avoir l'URL complète du fichier. Par exemple, pour le
   fichier Java, cette URL est
   https://gitlab.com/ablondin/inf1070-labos/raw/master/labo01/HelloWorld.java

<img src="open-raw.png" alt="" width="600"/>

## Exercice 3: Vim

Vim est un éditeur de texte en ligne de commande. Suivez le tutoriel
d'introduction en lançant la commande `vimtutor` dans un terminal.

Ensuite, récupérez le fichier `vimrc` disponible dans ce dépôt et placez-le
dans votre répertoire personnel, soit sous le nom `.vimrc` ou `.vim/vimrc`.  Il
s'agit d'un fichier de configuration de l'éditeur Vim qui vous permettra d'être
plus confortable au début. Plus vous serez à l'aise avec Vim, plus vous serez
appelés à le modifier pour y ajouter vos préférences.

## Exercice 4: Autres éditeurs de texte

Explorez brièvement les éditeurs de texte suivants.

1. Gedit, un éditeur de texte graphique, plus facile à utiliser quand on débute
   sur Linux.
2. Nano, un éditeur de texte en ligne de commande, un peu plus difficile à
   utiliser.

Notez que dans le cours, vous devrez être à l'aise avec au moins un éditeur de
texte en ligne de commande (Vim ou Nano), puisqu'il n'est pas toujours possible
(ou pratique) d'utiliser des éditeurs graphiques, en particulier lorsqu'on se
connecte sur un serveur.

## Exercice 5: Commandes

Pour terminer cette séance de laboratoire, vous allez vous familiariser avec la
commande `cal`, qui est très utile.

- Entrez `cal` dans un terminal et regardez le résultat.
- Ensuite, à l'aide de `man cal`, vérifiez comment vous pouvez afficher un
  calendrier pour l'année courante et non pas juste le mois courant.
- Sauvegardez le résultat de la commande précédente dans un fichier nommé
  `session-actuelle.txt` à l'aide d'une redirection.
- À l'aide de votre éditeur de texte préféré, modifiez le calendrier pour ne
  conserver que les 4 mois qui concernent la session actuelle.
- Ensuite, identifiez les dates importantes pour le cours INF1070 (dates des
  examens et dates des quiz) en ajoutant des étoiles autour. Par exemple, s'il
  y a un quiz le dimanche 16 septembre, alors on aurait quelque chose qui
  ressemble à

```sh
          September 2018     
 Su   Mo   Tu   We   Th   Fr   Sa  
                                1  
  2    3    4    5    6    7    8  
  9   10   11   12   13   14   15  
*16*  17   18   19   20   21   22  
 23   24   25   26   27   28   29  
 30                    
```
